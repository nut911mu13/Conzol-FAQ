import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueRouterMiddleware, { createMiddleware } from 'vue-router-middleware'
import Routes from './routers'

Vue.use(VueRouter)
Vue.config.productionTip = false

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
})

Vue.use(VueRouterMiddleware, { router })

createMiddleware('require-referer', (args, to, from, next) => {
  const authorized = sessionStorage.getItem("authorized")
  if ([`conzol.sansiri.com`].includes(document.referrer.split(/\//)[2])) {
    next()
  } else if (authorized) {
    next()
  } else {
    window.location.href = '/'
  }
})

new Vue({
  render: h => h(App),
  router: router,
}).$mount('#app')
