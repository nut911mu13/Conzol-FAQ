import { middleware } from 'vue-router-middleware'

import content from './components/module/content.vue'
// Single
import contentSingle from './components/module/content_single.vue'
// Double
import contentDouble from './components/module/content_double.vue'
// Triple
import contentTriple from './components/module/content_triple.vue'
// Forth
//import contentForth from './components/module/content_forth.vue'
// Fifth
//import contentFifth from './components/module/content_fifth.vue'


export default [
    {path:'/', component: content},
    ...middleware('require-referer', [
        // Single
        {path:'/faq', component: contentSingle},
        // double
        {path:'/frontoffice', component: contentDouble},
        // triple
        {path:'/backoffice', component: contentTriple},
        // forth
        //{path:'/frontofficepl', component: contentForth},
        // fifth
        //{path:'/backofficepl', component: contentFifth},
    ])
]